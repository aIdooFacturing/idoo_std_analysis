<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="device" var="device"></spring:message>
<spring:message code="cycle" var="cycle"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<%-- <link rel="stylesheet" href="${ctxPath }/css/style.css"> --%>
<%-- <link rel="stylesheet" href="${ctxPath }/css/jquery.loading.min.css"> --%>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<title>장비별 생산 실적</title>


<script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.min.js"></script>
<script src="https://code.highcharts.com/modules/xrange.js"></script>
<script src="https://code.highcharts.com/modules/annotations.js"></script>
         
<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>



<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery-rotate.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.loading.min.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	
}

.manual{
	margin-left: 5em;
	color: #BDBDBD;
}
</style> 

<script type="text/javascript">
	const loadPage = () =>{
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});

		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		chkBanner();
		
		createMenuTree("analysis", "production_Device");
	}
	
	$(function(){
		setEl();
		
		$.datepicker.setDefaults({
		    dateFormat: 'yy-mm-dd',
		    prevText: '이전 달',
		    nextText: '다음 달',
		    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    dayNames: ['일', '월', '화', '수', '목', '금', '토'],
		    dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
		    dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
		    showMonthAfterYear: true,
		    yearSuffix: '년'
		});
		
		$("#date").val(getToday().substring(0,10));
		$( "#date" ).datepicker({
			onSelect : function(e){
				//e == 날짜 
				$("#date").val(e);
			}
		})
		
		//순서 1. getdvcList() => gridTable() => getTable() => gridChart()
		
		//장비명 리스트 뽑아오기
		getdvcList();
		// 테이블 그리기
		//gridTable();
		// 차트 그리기
		//gridChart();
		
		//데이터 가져오기
//		getTable()
	})
	
	function getdvcList(){
		var url = ctxPath + "/getJigList4Report.do";
		
		var param = "shopId=" + shopId;

		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dvcList;
				var select = "";
				select +="<option id='0'>전체</option>"
				$(json).each(function(idx,data){
					data.name = decode(data.name)
					select +="<option id=" + data.dvcId +">" + data.name + "</option>"
				})
				$("#dvcList").html(select)
				
				//그리드 그리기
				gridTable();
				
			}
		})
	}
	
	var jsonData;
	var fixNum;
	function getTable(){
		var url = ctxPath + "/getProductionCnt.do";
		console.log($("#date").val().replace(/\./gi,"-"))
		var param = "date=" + $("#date").val().replace(/\./gi,"-") +
					"&dvcId=" + $("#dvcList option:selected").attr("id");

		/* for(i=1; i<=9; i++){
			eval("arr"+i+".no"+i+"=i");
		} */
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.wcList;
				$(json).each(function(idx,data){
					data.name = decode(data.name)
				})
				// 전역변수 chart 그리기위해
				jsonData = json;
				var tableList = [];

				console.log(json)
				
				// 배열 갯수 가로 컬럼수량 고정
				fixNum = 10;	//얘만 바꿔주면 컬럼 수량 조정 가능
				// 배열 시작 수량
				var Sta =0;
				// 배열 종료 수량
				var End = fixNum;
				// 총 json 길이
				var len = json.length;
				// 이중 for문 몇번 돌릴지 결정
				var howMany = Math.ceil(Number(len/fixNum)) ;
				// 구분선
				var line = 0;
				for(j=0; j<howMany; j++ ){
					// header columns number
					var HNo = 1;
					
					// 첫번째
					var arr1={};
					var arr2={};
					var arr3={};
					
					arr1.plan ="장비";
					arr2.plan ="계획";
					arr3.plan ="실적";

					for(i=Sta; i<End; i++){
						if(json[i]!=undefined){
							//장비
							eval("arr1.no"+ HNo +"=json["+i+"].name");
							//계획
							eval("arr2.no"+ HNo +"=json["+i+"].tgCyl");
							//수량
							eval("arr3.no"+ HNo +"=json["+i+"].cntCyl");
						}
						HNo ++;
						
					}
					Sta += fixNum;
					End += fixNum;
					tableList.push(arr1);
					tableList.push(arr2);
					tableList.push(arr3);
					line +=3;
					var row = 1;
				}

				kendodata = new kendo.data.DataSource({
					data: tableList,
					batch: true,
					editable: false,
					pageSize: 3,
					/* sort: [{
                    	field: "prdNo" , dir:"asc" 
                    },{
                    	field: "item" , dir:"asc"
                    }], */
					schema: {
						model: {
							id: "idx",
							fields: {
//  								num: { editable: false },
							}
						}
					}
				});
				
				kendotable.setDataSource(kendodata);
				

				$(".k-pager-numbers.k-reset").css({
					"font-size" : getElSize(36)
				})
				
				var kWidth = $(".k-pager-numbers.k-reset").width()
				
				$(".k-pager-numbers.k-reset").css({
					"margin-left" : getElSize(3708) - kWidth
				})
				
				
				gridChart(json,0)
				
// 				console.log(tableList)
			}
		})

	}
	
	var kendotable;
	function gridTable(){
		kendotable = $("#kendoTable").kendoGrid({
//			dataSource : list,
//			height: $("#DTable").height(),
			pageable:{
				info: false,
				buttonCount:5,
				previousNext: false,
				change:function(e){
					//페이지 클릭시 chart 다시 그려주기
					gridChart(jsonData,((e.index-1)*fixNum))
				}
			},
			dataBound:function(e){
				$(".k-grid tbody tr td").css({
					"font-size":getElSize(35.5),
					"text-align": "center",
				});
				$(".k-grid thead tr th").css({
					"text-align": "center",
					"font-size": getElSize(36),
				});
				$(".k-grid-header-wrap.k-auto-scrollable").css({
					"height" : getElSize(10)
				})
				$(".k-grid thead tr").css({
					"height" : 0
				});
				
				//추가 저장 버튼 css
				$("#detailBtn").css({
					"left": getElSize(2900),
					"top": getElSize(4),
				})
				
				$("tr[role='row']:contains('장비')").css({
					"background" :  "#6B6C7C",
					"font-weight" : "bold",
// 					"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black",
					"color" : "white"
				});	
/* 				$("td[role='gridcell']:contains('장비')").css({
					"background-color" :  "#6B6C7C",
					"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black",
					"color" : "white"
				});	 */
				$("td[role='gridcell']:contains('계획')").css({
					"background" :  "#0080FF",
					"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black",
					"color" : "white"
				});	
				$("td[role='gridcell']:contains('실적')").css({
					"background-color" :  "#A3D800",
					"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black",
					"color" : "white"
				});	

			},
			columns:[{
					field:"plan"
					,title:"　"
					,width: getElSize(110)
				},{
					field:"no1",title:"　",width: getElSize(250)

				},{
					field:"no2",
					title:"　",width: getElSize(250)
				},{
					field:"no3",
					title:"　",width: getElSize(250)
				},{
					field:"no4",
					title:"　",width: getElSize(250)
				},{
					field:"no5",
					title:"　",width: getElSize(250)
				},{
					field:"no6",
					title:"　"
 					,width: getElSize(250)
				},{
					field:"no7",
					title:"　"
 					,width: getElSize(250)
				},{
					field:"no8",
					title:"　"
 					,width: getElSize(250)
				},{
					field:"no9",
					title:"　"
 					,width: getElSize(250)
				},{
					field:"no10",
					title:"　"
 					,width: getElSize(250)
				}]
		}).data("kendoGrid")
		
		//데이터 가져오기
		getTable();
	}
	
//	abcT = '[{field:"code",title:"관리<br>코드",width: getElSize(140)},{field:"division",title:"측정기구분<br>측정기유형",template:"#=decode(division)#<br>#=decode(type)#",width: getElSize(500)},{field:"name",title:"측정기명",template:"#=decode(name)#",width: getElSize(300)},{field:"standard",title:"규격",template:"#=decode(standard)#",width: getElSize(260)},{field:"lastChk",title:"교정주기(개월)<br>최근교정일",template:"#=interval#<br>#=lastChk#",width: getElSize(300)},{field:"sDate",title:"차기 교정",width: getElSize(250)}]'

	var name
	function gridChart(json,startNum){
	
		// json merge 같은 key 값끼리 array로 합치는 방법
		var result = json.reduce(function(r, e) {
		  return Object.keys(e).forEach(function(k) {
		    if(!r[k]) r[k] = [].concat(e[k])
		    else r[k] = r[k].concat(e[k])
		  }), r
		}, {});
		
//		console.log(result)
		
		var wcName=[];
		var plan =[];
		var machinePerform =[];

		
		//기본 차트갯수를 10개로 잡기위해서 +10
		for(i=startNum; i< startNum+10; i++){
			// fixNum 갯수만큼만 생성하고 나머지는 공백값 넣기위한로직
			if(i >= (Number(startNum)+Number(fixNum))){
				wcName.push("")
				plan.push("")
				machinePerform.push("")
			}else{
				wcName.push(result.name[i])
				plan.push(result.tgCyl[i])
				machinePerform.push(result.cntCyl[i])
			}
		}
		
		$("#kendoChart").kendoChart({
			chartArea: {
				height: $("#DChart").height(),
				background:"#1E1E1E",
			},
			title: false,
            legend: {	//범례표?
             	labels:{
            		font:getElSize(43) + "px sans-serif",
            	},
            	stroke: {
            		width:100
            	},
            	position: "bottom",
            	orientation: "horizontal",
                offsetX: getElSize(1120),
//                offsetY: getElSize(800)
           
            },
            render: function(e) {	//범례 두께 조절
                var el = e.sender.element;
                el.find("text")
                    .parent()
                    .prev("path")
                    .attr("stroke-width", getElSize(20));
            },
			seriesDefaults: {	//데이터 기본값 ()
				gap: getElSize(2),
				type: "column" ,
				overlay: { gradient: "none" },
				spacing: getElSize(0.5),
 				labels:{
 					font:getElSize(45) + "px sans-serif",	//no working
 					margin:0,
 					padding:0
				}, 
/* 				visual: function (e) {
	                return createColumn(e.rect, e.options.color);
	            } */
			},	
			categoryAxis: {	//x축값
                categories: wcName,
                line: {
                    visible: true
                },
                labels:{
                	font:getElSize(39) + "px sans-serif",	//no working
				} 
            },
            tooltip: {	//커서 올리면 값나옴
                visible: true,
                format: "{0}%",
            	font:getElSize(48) + "px sans-serif",	//no working
                template: "#= series.name #: #= value #"
            },
            valueAxis: {	//간격 y축
                labels: {
                	font:getElSize(48) + "px sans-serif",	//no working
                    format: "{0}"
                }
//                majorUnit: 100	//간격
            },
            series: [{
                labels: {
					visible: true,
					background:"#0080FF",
//					rotation: 270
           	    },
					color : "#0080FF",
					name: "${plan}",
					data: plan
              },{
                labels: {
					visible: true,
					background:"#A3D800",
//					position: "top",
//					rotation: 270
				},
				color : "#A3D800",
                name: "장비실적",
                data: machinePerform
              }]
		})
	}
	
	var drawing = kendo.drawing;
	var geometry = kendo.geometry;

	function createColumn(rect, color) {
        var origin = rect.origin;
        var center = rect.center();
        var bottomRight = rect.bottomRight();
        var radiusX = rect.width() / 2;
        var radiusY = radiusX / 3;
        var gradient = new drawing.LinearGradient({
            stops: [{
                offset: 0,
                color: color
            }, {
                offset: 0.5,
                color: color,
                opacity: 0.9
            }, {
                offset: 0.5,
                color: color,
                opacity: 0.9
            }, {
                offset: 1,
                color: color
            }]
        });

        var path = new drawing.Path({
                fill: gradient,
                stroke: {
                    color: "none"
                }
            }).moveTo(origin.x, origin.y)
            .lineTo(origin.x, bottomRight.y)
            .arc(180, 0, radiusX, radiusY, true)
            .lineTo(bottomRight.x, origin.y)
            .arc(0, 180, radiusX, radiusY);

        var topArcGeometry = new geometry.Arc([center.x, origin.y], {
            startAngle: 0,
            endAngle: 360,
            radiusX: radiusX,
            radiusY: radiusY                
        });

        var topArc = new drawing.Arc(topArcGeometry, {
            fill: {
                color: color
            },
            stroke: {
                color: "#BDBDBD"
            }
        });
        var group = new drawing.Group();
        group.append(path, topArc);
        return group;
    }
	
	function setEl(){
		$("#Dheader").css({
			"height" : getElSize(170),
			"color" : "white",
// 			"background" : "red"
		})
		
		$("#DChart").css({
			"height" : getElSize(1230),
// 			"background" : "blue"
		})

		$("#DTable").css({
			"height" : getElSize(370),
// 			"background" : "green"
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(40),
		});

		$("button").hover(function(){
			$(this).css({
				"background-color" : "gray"
			})
		}, function(){
	        $(this).css({
				"background" : "#9B9B9B",
	        })
	    });
		
		$("button").css({
			"font-size" : getElSize(45),
			"color" : "white",
			"background" : "#9B9B9B",
			"cursor" : "pointer",
			"margin-left" : getElSize(13),
			"padding" : getElSize(13)
		});
		
		$("label").css({
			"font-size" : getElSize(45),
			"padding" : getElSize(13),
			"margin-left" : getElSize(13),
			"margin-right" : getElSize(13)
		})
		$(".menuCss").css({
			"font-size" : getElSize(45),
			"padding" : getElSize(13),
			"color" : "white",
			"background" : "black"
		})
		$("#dvcList").css({
			"width" : getElSize(450),
			"text-align" : "center"
		})
		
	}
</script>
</head>
<body>
	<div id="container">
		<div id="Dheader" style="display: table;">
			<div style="display: table-cell;vertical-align: middle;">
				<select id="dvcList" class="menuCss"><option>전체</option></select>
				<label> 날짜 </label>
				<input type="text" id="date" class="menuCss" readonly="readonly">
				<button onclick="getTable()">
					<img alt="" src="${ctxPath }/images/search.png" id="search" class='menu_left'>
					검색
				</button>
				<span class="manual">
					최신의 데이터를 보기위해서 검색버튼을 클릭해주세요.
				</span>
			</div>
		</div>
		<div id="DChart">
			<div id="kendoChart">
			</div>
		</div>
		<div id="DTable">
			<div id="kendoTable">
			</div>
		</div>
	 </div>
	 
	 <div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	