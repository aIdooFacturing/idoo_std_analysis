<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="device" var="device"></spring:message>
<spring:message code="cycle" var="cycle"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>

<%
	response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
	response.setHeader("pragma", "no-cache"); // HTTP 1.0
	response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<%-- <link rel="stylesheet" href="${ctxPath }/css/style.css"> --%>
<%-- <link rel="stylesheet" href="${ctxPath }/css/jquery.loading.min.css"> --%>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<title>TEMPLATE</title>


<script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript"
	src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript"
	src="${ctxPath }/js/multicolor_series.min.js"></script>
<script src="https://code.highcharts.com/modules/xrange.js"></script>
<script src="https://code.highcharts.com/modules/annotations.js"></script>

<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
<script src="http://kendo.cdn.telerik.com/2018.2.516/js/jszip.min.js"></script>



<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script type="text/javascript"
	src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery-rotate.js"></script>
<script type="text/javascript"
	src="${ctxPath }/js/jquery.loading.min.js"></script>
<style>
* {
	margin: 0px;
	padding: 0px;
}

body {
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow: hidden;
	background-color: black;
}
</style>

<script type="text/javascript">

	const loadPage = () =>{
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});

		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		chkBanner();
		
		createMenuTree("analysis", "production_Daily");
	}
	
	$(function(){
		setEl();

		$("#sDate").val(moment().subtract(9, 'day').format("YYYY-MM-DD"));
		$("#eDate").val(moment().format("YYYY-MM-DD"));
		
		$( "#sDate" ).datepicker({
			onSelect : function(e){
				//e == 날짜 
				$("#sDate").val(e);
			}
		})
		
		$( "#eDate" ).datepicker({
			onSelect : function(e){
				//e == 날짜 
				$("#eDate").val(e);
			}
		})
		
		//장비명 리스트 뽑아오기
		getdvcList();
// 		gridTable();

	})
	
	function setEl(){
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(40),
		});
		
		$("#div1").css({
			"height" : getElSize(150)
			,"width" : contentWidth
		})
		
		$(".btn").css({
			"font-size" : getElSize(45)
			,"padding" : getElSize(10)
		})
		
		$("button").css({
			"background-color" : "#9B9B9B"
		})
		
		$("button").hover(function(){
			$(this).css({
				"background-color" : "gray"
			})
		}, function(){
	        $(this).css({
				"background" : "#9B9B9B",
	        })
	    });
	}
	
	function getdvcList(){
		var url = ctxPath + "/getJigList4Report.do";
		
		var param = "shopId=" + shopId;

		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dvcList;
				var select = "";
//				select +="<option id='ALL'>전체</option>"
				$(json).each(function(idx,data){
					data.name = decode(data.name)
					select +="<option id=" + data.dvcId +">" + data.name + "</option>"
				})
				$("#dvcList").html(select)
				
				//그리드 그리기
				gridTable();
				
			}
		})
	}
	
	var kendotable;
	function gridTable(){
		kendotable = $("#grid").kendoGrid({
			/* toolbar:[{
				template : "<span class='k-button menuCss'> 장비  <input id='dvcList'> </span>"	
			},{
				template : "<span class='k-button menuCss'> 구분  <input id='dvcList'> </span>"	
			},{
				template : "<span class='k-button menuCss'> 기간 1 <input id='dvcList'> ~ <input id='eDate'> </span>"	
			}]*/
			height : getElSize(1650)
			,excel:{
				fileName:"일자별" + getToday().substring(0,10) + ".xlsx"
			} 
			,columns:[{
				field : "date"
				,title : "일자"
			},{
				title : "주간"
				,columns:[{
					field : "capa"
					,title : "Capa"
				},{
					field : "tgCyl"
					,title : "계획"
				},{
					field : "cntD"
					,title : "실적"
				},{
					field : "goalRatioD"
					,title : "달성율"
				}]
			},{
				title : "야간"
				,columns:[{
					field : "capa"
					,title : "Capa"
				},{
					field : "tgCyl"
					,title : "계획"
				},{
					field : "cntN"
					,title : "실적"
				},{
					field : "goalRatioN"
					,title : "달성율"
				}]
			}]
		}).data("kendoGrid")
		
		getTable()
	}
	
	function getTable(){
		var url = ctxPath + "/getProductionDaily.do";

		var param = "sDate=" + $("#sDate").val() +
					"&eDate=" + $("#eDate").val() +
					"&dvcId=" + $("#dvcList option:selected").attr("id") +
					"&plan=" + $("#planS option:selected").attr("id") +
					"&name=" + $("#dvcList").val();
		// 
		$.showLoading()
		console.log(param)
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				$.hideLoading()
				var json = data.dataList;
				console.log(json)
				kendodata = new kendo.data.DataSource({
					data: json,
					batch: true,
					editable: false,
					pageSize: json.length,
					/* sort: [{
                    	field: "prdNo" , dir:"asc" 
                    },{
                    	field: "item" , dir:"asc"
                    }], */
					schema: {
						model: {
							id: "idx",
							fields: {
//  								num: { editable: false },
							}
						}
					}
				});
				
				kendotable.setDataSource(kendodata);
				
				$("tbody tr").css("color","white")
				$(".k-alt").css("color", "black")
				//
			}
		})
	}
</script>
</head>
<body>
	<div id="container">
		<div id="div1" style="display: table;">
			<div style="display: table-cell; vertical-align: middle;">
				<label> 장비 </label> <select id="dvcList" class="menuCss"
					style="padding-right: 4%;"></select> <label> 구분 </label> <select id ="planS"
					class="menuCss" style="padding-right: 4%;"><option id=1>일자료</option>
					<option id=2>월자료</option></select> <label> 기간 </label> <input type="text"
					id="sDate" class="menuCss date" readonly="readonly"> ~ <input
					type="text" id="eDate" class="menuCss date" readonly="readonly">
				<button onclick="getTable()" class="btn">
					<img alt="" src="${ctxPath }/images/search.png" id="search"
						class='menu_left'> 검색
				</button>

			</div>
		</div>
		<div id="grid"></div>
	</div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>
