const loadPage = () =>{
	createMenuTree("analysis", "Performance_Report_Daily")
	
	createSearchDiv()
	getDvcList();
	
	$("#intro").css({
		"position" : "absolute",
		"bottom" : 0 + marginHeight,
		"font-size" : getElSize(140),
		"font-weight" : "bolder",
		"z-index" : 9999
	});

	
	$("#intro_back").css({
		"width" : originWidth,
		"display" : "none",
		"height" : getElSize(180),
		"opacity" : 0.5,
		"position" : "absolute",
		"background-color" : "black",
		"bottom" : 0 + marginHeight,
		"z-index" : 9999
	})
	
	$("#intro").css({
		"font-size" : getElSize(100)
	});
	
	$(".text").css({
		"color" : "white"
	})
	chkBanner();
}


var barChart;
var maxBar = 10;

var wcDataList = new Array();
var dateList = new Array();
var wcList = new Array();
var wcName = new Array();
var maxPage;
var cBarPoint = 0;

let c_page = 1;
const max_row = 10;

let dvcListLength = 0

var lineName ="";
var lineTarget="";
var lineIncycle="";
var lineWait ="";
var lineAlarm ="";
var lineOff ="";
var linePrc ="";

const getAllDvcList = () =>{
	var dvc = $("#group").val();
	var sDate = $("#sDate").val();
	var eDate = $("#eDate").val();
	
	var url = ctxPath + "/getWcDataByDvc.do";
	var param = "dvcId=" + dvc + 
				"&jig=" + $("#jigGroup").val() +
				"&WCCD=" + $("#wccdGroup").val() +
				"&sDate=" + sDate + 
				"&eDate=" + eDate + 
				"&shopId=" + shopId +
				"&maxRow=" + 200 + 
				"&offset=0";
				
	$.showLoading();
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.wcList;
			
			lineName ="구분";
			lineIncycle ="가동시간(h)";
			lineWait ="대기(h)";
			lineAlarm ="알람(h)";
			lineOff ="Power Off(h)";
			lineTarget ="목표시간(h)";
			linePrc ="생산수량(EA)";

			var start = new Date($("#sDate").val());
			var end = new Date($("#eDate").val());
			
			var n = (end - start)/(24 * 3600 * 1000)+1;

			$(json).each(function(idx,data){
				lineName += ", " + data.workDate
				lineTarget += "," + (Number(data.target_time)/60/60).toFixed(1)
				lineIncycle += "," + (Number(data.inCycle_time)/60/60).toFixed(1)
				lineWait += "," + (Number(data.wait_time)/60/60).toFixed(1)
				lineAlarm += "," + (Number(data.alarm_time)/60/60).toFixed(1)
				lineOff += "," + ((Number(data.target_time)/60/60).toFixed(1) - (Number(data.inCycle_time)/60/60).toFixed(1) - (Number(data.wait_time)/60/60).toFixed(1) - (Number(data.alarm_time)/60/60).toFixed(1)).toFixed(1)
				linePrc += "," + data.prdcCnt
			})
			
			console.log("--total--");
			console.log(param)
			console.log(data);
			
			dvcListLength = json.length;
			
			showWcDatabyDvc()
			
			createPager(c_page)
			
			$.hideLoading();
		}
	});
	
}

const createPager = (page) =>{
	let pageCnt = Math.ceil(dvcListLength/max_row) 
	
	$(".pager").remove()
	for(i = pageCnt, j = 0; i > 0; i--, j++){
		//getElSize(1864 * 2) + marginWidth
		let pagerWidth = getElSize(32 * 2)
		let margin = getElSize(16 * 2)
		
		let left = (j * -margin) + (j * -pagerWidth) +  getElSize(1864 * 2) + marginWidth
		
		const pager = 
			`
				<div id="page${i}"
					class="pager"
					onclick="moveBarChartPage(${i});"
					style=
						"
							position : absolute
							; z-index : 2
							; width : ${pagerWidth}px
							; height : ${getElSize(32 * 2)}px
							; top :  ${getElSize(1024 * 2) + marginHeight}px
							; left : ${left}px
							; background-color : #2B2D32
							; color : white
							; cursor : pointer
							; text-align : center
							; display : table
							
							; font-size : ${getElSize(18 * 2)}px
						"
				>
					<span style="display:table-cell; vertical-align:middle">${i}</span>
					
				</div>
			`
			
		$("#container").append(pager)	
			
	}
	
	$("#page" + page).css({
		"color" : "black",
		"background-color" : "#6699F0"
	})
}

const moveBarChartPage = (page) =>{
	$.showLoading();
	
	c_page = page
	getAllDvcList()
	
	createPager(c_page)
}

const getDvcList = () => {
	var url = ctxPath + "/getMatInfo.do";

	var param = "shopId=" + shopId;
	
	$.showLoading();
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(dataList){
			
			
			var json = dataList.dataList;
			var jigList = dataList.jigList;
			var wccdList = dataList.wccdList;
			
			console.log("--select list call--")
			
			let options = "<option value='0'>전체</option>";
			let jigoptions = "<option value='ALL'>전체</option>";
			let wccdoptions = "<option value='ALL'>전체</option>";
			
			$(json).each(function(idx, data){
				options += "<option value='" + data.dvcId + "'>" + decodeURIComponent(data.prdNo).replace(/\+/gi," ") + "</option>";  
			});
			
			$(jigList).each(function(idx,data){
				jigoptions += "<option value='" + data.category + "'>" + decodeURIComponent(data.category).replace(/\+/gi," ") + "</option>";  
			})
			
			$(wccdList).each(function(idx,data){
				wccdoptions += "<option value='" + data.category + "'>" + decodeURIComponent(data.category).replace(/\+/gi," ") + "</option>";
			});
			
			$("#group").html(options)
			$("#jigGroup").html(jigoptions)
			$("#wccdGroup").html(wccdoptions)
			
			
			
			
			getAllDvcList()
			
			//showWcDatabyDvc()
		}
	});
};

const showWcDatabyDvc = (ty) =>{
	var dvc = $("#group").val();
	var sDate = $("#sDate").val();
	var eDate = $("#eDate").val();
	
	var url = ctxPath + "/getWcDataByDvc.do";
	var param = "dvcId=" + dvc + 
				"&jig=" + $("#jigGroup").val() +
				"&WCCD=" + $("#wccdGroup").val() +
				"&sDate=" + sDate + 
				"&eDate=" + eDate + 
				"&shopId=" + shopId +
				"&maxRow=" + max_row + 
				"&offset=" + ((c_page-1)*max_row);
	
	
	console.log("dvc is : " + dvc)
	console.log("ty is : " + ty)
	
	if(ty=="jig"){
		$("#wc_sdate").val($("#jig_sdate").val());
		$("#wc_edate").val($("#jig_edate").val());		
	};
	
	console.log(param)
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.wcList;
			
			console.log("----------")
			console.log(json.length)
			if(json.length==0){
				$("#kendoChart").empty();
				$("#kendoChart").css("height",0)
				$(".chartTable").remove();
				table = "<div class='chartTable'>장비,가동 기간을 다시 확인해주세요.</div>"
				$("#tableContainer").append(table)
				
				$(".chartTable").css({
				    "background" : "black",
					"margin-left" : "15%",
					"margin-top" : "10%",
					"font-size" : "300%"
				})
				return;
			}
			

			var start = new Date(sDate);
			var end = new Date(eDate);
			var n = (end - start)/(24 * 3600 * 1000)+1;

			var tr = "";

			wcList = new Array();
			var wc = new Array();
			
			wc.push(division);
			wc.push(ophour +" (h)");
			wc.push(wait +" (h)");
			wc.push(stop +" (h)");
			wc.push(noconnection +" (h)");
			wc.push(prdctCnt +" (EA)");
			
			wcList.push(wc);
			
			dateList = new Array();
			inCycleBar = new Array();
			waitBar = new Array();
			alarmBar = new Array();
			noConnBar = new Array();
			$(json).each(function(idx, data){
				dateList.push(data.workDate.substr(5).replace("-","/"));
				tmpArray = dateList;
				
				var wc = new Array();
				wc.push(data.workDate.substr(5).replace("-","/"));
				console.log(data.inCycle_time)
				wc.push(Number(data.target_time));
				wc.push(Number(data.inCycle_time));
				wc.push(Number(data.wait_time));
				wc.push(Number(data.alarm_time));
				wc.push(Number(Number(data.target_time - (Number(data.inCycle_time) + Number(data.wait_time) + Number(data.alarm_time) )).toFixed(1)));
//				wc.push(Number(Number(24 * data.cnt * 60 * 60 - (Number(data.inCycle_time) + Number(data.wait_time) + Number(data.alarm_time) )).toFixed(1)));
				wc.push(Number(data.prdcCnt * 3600));
				
				wcList.push(wc);
				tmpWcList = wcList;
				
				var incycle = Number(Number(data.inCycle_time/60/60).toFixed(1));
				var wait = Number(Number(data.wait_time/60/60).toFixed(1));
				var alarm = Number(Number(data.alarm_time/60/60).toFixed(1));
				var noconn = Number(24 * data.cnt - (incycle + wait + alarm)).toFixed(1);
				
				inCycleBar.push(incycle);
				waitBar.push(wait);
				alarmBar.push(alarm);
				noConnBar.push(noconn);
				tmpInCycleBar = inCycleBar;
				tmpWaitBar = waitBar;
				tmpAlarmBar = alarmBar;
				tmpNoConnBar = noConnBar;
			});
		
			var blank = maxBar - json.length
			maxPage = json.length - maxBar; 
				
			for(var i = 0; i < blank; i++){
				dateList.push("");
				var wc = new Array();
				wc.push("____");
				wc.push("");
				wc.push("");
				wc.push("");
				wc.push("");
				wc.push("");
				
				wcList.push(wc);
				
				inCycleBar.push(0);
				waitBar.push(0);
				alarmBar.push(0);
				noConnBar.push(0);
			};
			
			for(var i = 0; i < maxBar - json.length % 10; i++){
				dateList.push("");
				var wc = new Array();
				wc.push("____");
				wc.push("");
				wc.push("");
				wc.push("");
				wc.push("");
				wc.push("");
				
				wcList.push(wc);
				
				inCycleBar.push(0);
				waitBar.push(0);
				alarmBar.push(0);
				noConnBar.push(0);
			};
			
			if(json.length > maxBar){
				overBar = true;
				
			};
			
			reArrangeArray();
			
			$(".chartTable").remove();
			var table = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
			
			for(var i = 0; i < wcList[0].length; i++){
				table += "<tr class='contentTr'>";
				let color = ""
				var bgColor;
				if(i==1){
					bgColor = "#00B700";
					color = "black";
				}else if(i==2){
					bgColor = incycleColor
					color = "black";
				}else if(i==3){
					bgColor = "#FF9100";
					color = waitColor
				}else if(i==4){
					bgColor = "#C41C00";
					color = alarmColor
				}else if(i==5){
					bgColor = "#6B6C7C";
					color = noConnColor
				}else if(i == 6){
					bgColor = "#6699F0";
				}else{
					bgColor = "#323232";
				}
				
				color = "black"
				for(var j = 0; j < wcList.length; j++){
					if(j==0){
						table += "<td style='font-weight: bolder;  background-color: " + bgColor + "; color:" + color + "; height : " + getElSize(40 * 2) + "; width:" + getElSize(250) + "'>" + wcList[j][i] + "</td>";
					}else if(j==0 || i==0){
						table += "<td style='font-weight: bolder; background-color: background-color: " + bgColor + "; color:" + color + "; width:" + chartWidth/(maxBar-1) + "' onclick='toDailyChart(\"" + wcList[j][i] + "\")'>" + wcList[j][i] + "</td>";
					}else {
						var n;
						if(typeof(wcList[j][i])=="number"){
							if(i != 6){
								n = Number(wcList[j][i]/60/60).toFixed(1)
							}else{
								n = Number(wcList[j][i]/60/60)
							}

						}else{
							n = "";
						};
						table += "<td>" + n + "</td>";
					};
				};
				table += "</tr>";
			};
			
			table += "</table>";
			$("#tableContainer").append(table)
			
			//setEl();
			chart("chart");
			//addSeries();
			$("#tableContainer td").css({
				"font-size" : getElSize(18 * 2),
				"color" : "black"
			})
			
			$(".chartTable tr:eq(0) td").css({
				"background":"#353542",
				"color" : "white"
			})
			
			$(".chartTable tr:eq(1) td").not(":nth(0)").css({
				"background":"#DCDCDC",
			})
			
			$(".chartTable tr:eq(3) td").not(":nth(0)").css({
				"background":"#DCDCDC",
			})
			
			$(".chartTable tr:eq(5) td").not(":nth(0)").css({
				"background":"#DCDCDC",
			})
			
			$(".chartTable tr:eq(2) td").not(":nth(0)").css({
				"background":"#F0F0F0",
			})
			
			$(".chartTable tr:eq(4) td").not(":nth(0)").css({
				"background":"#F0F0F0",
			})

			$(".chartTable tr:eq(6) td").not(":nth(0)").css({
				"background":"#F0F0F0",
			})
			
			
			$(".chartTable tr td").css("width","9%")
			//$("#tableContainer").css("margin-top",$("#chart").offset().top + $("#chart").height() - $(".mainTable").height() + getElSize(50))
			
			$.hideLoading();
		}
	});
};

var xAxis = new Array();
var barChart;
const chart = (id) =>{
	var sDate = window.localStorage.getItem("sDate");
	var eDate = window.localStorage.getItem("eDate");
	
	var date = new Date(sDate);
	
	for(var i=0;i<dateList.length; i++){
		if(dateList[i]==""){
			console.log('zz')
			inCycleBar[i]=null
			waitBar[i]=null
			alarmBar[i]=null
		}
	}
	
	$("#kendoChart").kendoChart({
		chartArea: {
			height: getElSize(390 * 2),
			margin : {left : $("#tableContainer table tr:nth(0) td:nth(1)").offset().left - marginWidth},
			background:"#000000",
		},
		title: false,
        legend: {	//범례표?
         	labels:{
        		font:getElSize(48) + "px sans-serif",
        		color:"white"
        	},
        	stroke: {
        		width: getElSize(32 * 2)
        	},
        	position: "bottom",
        	orientation: "horizontal",
            offsetX: getElSize(1010),
//            offsetY: getElSize(800)
            visible : false
       
        },
        render: function(e) {	//범례 두께 조절
            var el = e.sender.element;
            el.find("text")
                .parent()
                .prev("path")
                .attr("stroke-width", getElSize(20));
        },
		seriesDefaults: {	//데이터 기본값 ()
			gap: getElSize(5),
			type: "column" ,
			stack:true,
			spacing: getElSize(1),
				labels:{
					font:getElSize(18 * 2) + "px sans-serif",	//no working
					margin:0,
					padding:0
			}/* , 
			visual: function (e) {
                return createColumn(e.rect, e.options.color);
            } */
		},	
		categoryAxis: {	//x축값
            categories: dateList,
            majorGridLines: {
                color: "rgba(0,0,0,0)"
              },
            line: {
                visible: false
            },
            labels:{
            	font:getElSize(14 * 2) + "px sans-serif",	
            	color:"white"
			} 
        },
        tooltip: {	//커서 올리면 값나옴
            visible: true,
            format: "{0}%",
            template: "#= series.name #: #= value #"
        },
        valueAxis: {	//간격 y축
        	majorGridLines: {
                color: "#353542"
              },
            labels: {
                format: "{0}",
                font:getElSize(14 * 2) + "px sans-serif",
                color:"white"
            }
//            majorUnit: 100	//간격
        },

        
        series: [/* {	//데이터
            labels: {
                visible: true,
                color: "black",	
                background:"gray",
                font:getElSize(60) + "px sans-serif",	
          	    position: "center",
              },
              color : "gray",
              name: "noConnBar",
              data: noConnBar		
            }, */{	//데이터
            		overlay: { gradient: "none" },
            		labels: {
            			visible: true,
            			color: "white",	
            			background:"#A3D800",
            			font:getElSize(18 * 2) + "px sans-serif",	
            			position: "center",
            			margin: {
            				right : 45
            			}
//          	    	margin:10
            		},
            		color : incycleColor,
            		name: ophour,
            		data: inCycleBar		
            	},{	//데이터
            		overlay: { gradient: "none" },
            		labels: {
            			visible: true,
            			background:"#FF9100",
            			font:getElSize(18 * 2) + "px sans-serif",	
            			position: "center",
            			color: "white",
            			margin: {
	//                    	bottom : 100,
	                    	left : 45
            			}
            		},
                    color : waitColor,
                    name: wait,
                    data: waitBar		
            	},{	//데이터
            		overlay: { gradient: "none" },
            		labels: {
            			visible: true,
            			background:"#C41C00",
            			font:getElSize(18 * 2) + "px sans-serif",	
//                    	position: "center",
            			color: "white",
            			margin: {
//                    		bottom : 100,
            				right : 45
            			}
            		},
					color : alarmColor,
					name: stop,
					data: alarmBar		
            	}]
	})
	
	barChart = $("#" + id).highcharts();
}

var jig = window.localStorage.getItem("jig");


var chartWidth;
var maxBar = 10;
var tmpArray = new Array();
var tmpInCycleBar = new Array();
var tmpWaitBar = new Array();
var tmpAlarmBar = new Array();
var tmpNoConnBar = new Array();
var tmpWcName = new Array();
var tmpWcList = new Array();

const reArrangeArray = () =>{
	dateList = new Array();
	inCycleBar = new Array();
	waitBar = new Array();
	alarmBar = new Array();
	noConnBar = new Array();
	wcName = new Array();
	wcList = new Array();
	
	for(var i = cBarPoint; i < (maxBar+cBarPoint); i++){
		dateList[i-cBarPoint] = tmpArray[i];
		inCycleBar[i-cBarPoint] = tmpInCycleBar[i];
		waitBar[i-cBarPoint] = tmpWaitBar[i];
		alarmBar[i-cBarPoint] = tmpAlarmBar[i];
		noConnBar[i-cBarPoint] = tmpNoConnBar[i];
		wcName[i-cBarPoint] = tmpWcName[i];
	};
	
	for(var i = cBarPoint; i < (maxBar+cBarPoint+1); i++){
		wcList[i-cBarPoint] = tmpWcList[i];	
	};
	var wc = new Array();
	wc.push(division);
	wc.push("목표 시간 (h)")
	wc.push(ophour +" (h)");
	wc.push(wait +" (h)");
	wc.push(stop +" (h)");
	wc.push(noconnection +" (h)");
	wc.push(prdctCnt +" (EA)");
	
	
	//wcList.push(wc);
	wcList[0] = wc;
	
	
};

var cPage = 1;

const createSearchDiv = () =>{
	const div = 
		`
			<div
				style=
					"
						position : absolute
						; z-index : 2
						; width : ${getElSize(1888 * 2)}px
						; height : ${getElSize(64 * 2)}px
						; top : ${getElSize(175 * 2) + marginHeight}px
						; left : ${getElSize(16 * 2) + marginWidth}px
						; background-color : #2B2D32 
					"
			>
				<span class="text"> 장비 </span> <select id="group"
					style=
						"
							margin-top : ${getElSize(12 * 2)}px
							; margin-left : ${getElSize(12 * 2)}px
							; margin-right : ${getElSize(22 * 3)}px
							; width : ${getElSize(550)}px
							
						"
				>
				</select>
				
				<span class="text"> JIG </span>
				<select id="jigGroup"
					style=
						"
							margin-top : ${getElSize(12 * 2)}px
							; margin-left : ${getElSize(12 * 2)}px
							; margin-right : ${getElSize(22 * 3)}px
							; width : ${getElSize(350)}px
							
						"
				>
				</select>
				
				<span class="text"> WC_CD </span>
				<select id="wccdGroup"
					style=
						"
							margin-top : ${getElSize(12 * 2)}px
							; margin-left : ${getElSize(12 * 2)}px
							; margin-right : ${getElSize(22 * 3)}px
							; width : ${getElSize(350)}px
							
						"
				>
				</select>
				
				<span 
					style=
						"
							z-index : 2
							; color : white
							; font-size : ${getElSize(24 * 2)}px
							; top : ${getElSize(22 * 2)}px
							; line-height : ${getElSize(24 * 2)}px
						"
				>${op_period}</span>
				
				<input type="date"
					id="sDate"
					style=
						"
							z-index : 2
							; top : ${getElSize(14 * 2)}px
							
						"
				>
				
				<input type="date"
					id="eDate"
					style=
						"
							z-index : 2
							; top : ${getElSize(14 * 2)}px
							;
						"
				>
				
				<div
					onclick="getAllDvcList();"
					style=
						"
							position : absolute
							; z-index : 2
							; width : ${getElSize(120 * 2)}px
							; height : ${getElSize(40 * 2)}px
							; background-color : #9B9B9B
							; font-size : ${getElSize(24 * 2)}px
							; top : ${getElSize(12 * 2)}px
							; border-radius : ${getElSize(2 * 2)}px
							; text-align:center
							; left : ${getElSize(1650 * 2)}px
							; display : table
							; cursor : pointer
						"
				>
					<img src="${ctxPath}/images/FL/default/ico_search.svg" style="width : ${getElSize(23 * 2)}px; margin:${getElSize(8 * 2)}px; margin-right:0px;  margin-left:0px">
					<span style="display:table-cell; vertical-align:middle">${search}</span>
				</div>
				
			</div>
		`
	
	const graphDiv = 
		`
			<div
				id="kendoChart"
				style=
					"
						position : absolute
						; z-index : 2
						; width : ${contentWidth}px
						; height : ${getElSize(428 * 2)}px
						; top : ${getElSize(274 * 2) + marginHeight - $("#container").offset().top}px
					"
			></div>
		`
		
		
	const tableContainer = 
		`
			<div 
				style=
					"
						position : absolute
						; z-index : 2
						; width : ${getElSize(1888 * 2)}px
						; height : ${getElSize(282 * 2)}px
						; background-color : #1E1E23
						; top : ${getElSize(695 * 2) + marginHeight}px
						; left : ${getElSize(16 * 2) + marginWidth}px
					"
			>
				<div id="tableContainer" class="tableContainer" style="margin : ${getElSize(16 * 2)}px"></div>
				
			
			</div>
		`
		
	const pager = 
		`
			<div 
				style=
					"
						position : absolute
						; z-index : 2
						; width : ${getElSize(1888 * 2)}px
						; height : ${getElSize(48 * 2)}px
						; background-color : #1E1E23
						; top : ${getElSize(1016 * 2) + marginHeight}px
						; left : ${getElSize(16 * 2) + marginWidth}px
					"
			>
				<button id ="excelDown" onclick = "download()"
					style =
						"
							height:100%;
							font-size:100%; 
							background:lightsteelblue; 
							border:0; 
							outline:0; 
							padding : ${getElSize(25)}px; 
							border-radius : ${getElSize(7)}px;
							cursor : pointer;
						"
				>
					Excel DownLoad
				</button>
			</div>
		`
		
	$("#container").append(div, graphDiv, tableContainer, pager)	
	
	$("#sDate").val(caldate(7))
	$("#eDate").val(caldate(0))
	setDateDesign()
}

const goGraph = () => {
	location.href = ctxPath + "/Performance_Report_Chart_table.do"
};


function download(){
	var id = this.id;
	var sDate, eDate;
	var csvOutput;
	today = "2020-02-14"
	sDate = $("#sDate").val();
	eDate = $("#eDate").val();
	var excelData = "";

	excelData = lineName + "LINE"
	excelData += lineTarget + "LINE"
	excelData += lineIncycle + "LINE"
	excelData += lineWait + "LINE"
	excelData += lineAlarm + "LINE"
	excelData += lineOff + "LINE"
	excelData += linePrc + "LINE"
//	lineName ="";
//	lineIncycle ="";
//	lineWait ="";
//	lineAlarm =
	csvOutput = excelData;
	
	csvOutput = csvOutput.replace(/\n/gi,"");
	
	console.log("dvcId : " + $("#dvcId option:selected").text());
	
	$("#excelTitle").val("daily_performance");
	
	f.csv.value=encodeURIComponent(csvOutput);
	f.startDate.value = sDate + " ~ " + eDate;
	f.endDate.value = eDate;
	f.submit();
}

