package com.unomic.dulink.chart.service;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.unomic.dulink.chart.domain.ChartVo;
 

@Service
@Repository
public class ChartServiceImpl extends SqlSessionDaoSupport implements ChartService{
	private final static String namespace= "com.unomic.dulink.chart.";
	
	@Autowired
	@Resource(name="sqlSessionTemplate_app_server")
	private SqlSession app_server_sql;
	
	@Override
	public String getProductionCnt(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> wcList = sql.selectList(namespace + "getProductionCnt", chartVo);
		
		List list = new ArrayList();
		
		for(int i = 0; i < wcList.size(); i++){
			Map map = new HashMap();
			
			map.put("dvcId", wcList.get(i).getDvcId());
			map.put("name", URLEncoder.encode(wcList.get(i).getName(), "utf-8"));
//			map.put("tgCyl", i*100);
			map.put("tgCyl", wcList.get(i).getTgCyl());
//			map.put("cntCyl", i*10);
			map.put("cntCyl", wcList.get(i).getCntCyl());
			
			list.add(map);
		};
		
		
		Map dataMap = new HashMap();
		dataMap.put("wcList", list);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String getWcDataByDvc(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> wcList = sql.selectList(namespace + "getWcDataByDvc", chartVo);

		List list = new ArrayList();

		for (int i = 0; i < wcList.size(); i++) {
			Map map = new HashMap();

			map.put("target_time", wcList.get(i).getTargetDateTime());
			map.put("inCycle_time", wcList.get(i).getInCycleTime());
			map.put("wait_time", wcList.get(i).getWaitTime());
			map.put("alarm_time", wcList.get(i).getAlarmTime());
			map.put("noConnTime", wcList.get(i).getNoConnectionTime());
			map.put("workDate", wcList.get(i).getWorkDate());
			map.put("prdcCnt", wcList.get(i).getPrdcCnt());
			map.put("cnt", wcList.get(i).getCnt());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("wcList", list);

		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getProductionDaily(ChartVo chartVo) throws Exception{
		SqlSession sql = getSqlSession();
		// TODO Auto-generated method stub
		List<ChartVo> dataList = new ArrayList<ChartVo>();
		try {
			dataList = sql.selectList(namespace + "getProductionDaily", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List list = new ArrayList();
		
		for (int i = 0; i < dataList.size(); i++) {

			Map map = new HashMap();
			
			map.put("date", dataList.get(i).getTgDate());
			map.put("capa", dataList.get(i).getCapa());
			map.put("tgCyl", dataList.get(i).getTgCyl());
			map.put("cntD", dataList.get(i).getCntD());
			map.put("goalRatioD", dataList.get(i).getGoalRatioD());
			map.put("cntN", dataList.get(i).getCntN());
			map.put("goalRatioN", dataList.get(i).getGoalRatioN());
			
			list.add(map);
		}
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		
		return str;
	}
	
	@Override
	public String getJigList4Report(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dvcList = sql.selectList(namespace + "getJigList4Report", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dvcList.size(); i++) {
			Map map = new HashMap();
			map.put("name", URLEncoder.encode(dvcList.get(i).getName(), "utf-8"));
			map.put("dvcId", dvcList.get(i).getDvcId());
			list.add(map);
		}
		;

		Map dvcMap = new HashMap();
		dvcMap.put("dvcList", list);

		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dvcMap);

		return str;
	}
	
	@Override
	public String getTableData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		List list = new ArrayList();

		try {
			List<ChartVo> dataList = sql.selectList(namespace + "getDataList", chartVo);

		
			for (int i = 0; i < dataList.size(); i++) {
				Map map = new HashMap();
//				URLDecoder.decode(
				map.put("name", URLEncoder.encode(dataList.get(i).getName()));
				map.put("target_time", dataList.get(i).getTargetDateTime());
				map.put("inCycle_time", dataList.get(i).getInCycleTime());
				map.put("wait_time", dataList.get(i).getWaitTime());
				map.put("alarm_time", dataList.get(i).getAlarmTime());
				map.put("noConnTime", dataList.get(i).getNoConnectionTime());
				// map.put("line", dataList.get(i).getLine());

				list.add(map);
			};
			
			str = "success";
		} catch (Exception e) {
			str = "fail";
			e.printStackTrace();
			// TODO: handle exception
		}

		Map dataMap = new HashMap();
		dataMap.put("tableData", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
		
	}

	@Override
	public String getMatInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getMatInfo", chartVo);
		List<ChartVo> jig = sql.selectList(namespace + "getJIGList", chartVo);
		List<ChartVo> wccd = sql.selectList(namespace + "getWCCDList", chartVo);

		List list = new ArrayList<ChartVo>();
		List jiglist = new ArrayList<ChartVo>();
		List wccdlist = new ArrayList<ChartVo>();

		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("dvcId", dataList.get(i).getDvcId());
//			map.put("ITEMNO", URLEncoder.encode(dataList.get(i).getITEMNO(), "utf-8"));
//			map.put("RWMATNO", URLEncoder.encode(dataList.get(i).getRWMATNO(), "utf-8"));
//			map.put("prce", Integer.parseInt(dataList.get(i).getPRCE()));
			list.add(map);
		};
		
		for(int i = 0; i<jig.size(); i++) {
			Map map = new HashMap();
			map.put("category", URLEncoder.encode(jig.get(i).getJig(), "utf-8"));
			jiglist.add(map);
		}

		for(int i = 0; i<wccd.size(); i++) {
			Map map = new HashMap();
			map.put("category", URLEncoder.encode(wccd.get(i).getWCCD(), "utf-8"));
			wccdlist.add(map);
		}

		Map dataMap = new HashMap();
		
		dataMap.put("dataList", list);
		dataMap.put("jigList", jiglist);
		dataMap.put("wccdList", wccdlist);
		
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getBarChartDvcId(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dvcIdList = sql.selectList(namespace + "getBarChartDvcId", chartVo);

		List list = new ArrayList();

		for (int i = 0; i < dvcIdList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dvcIdList.get(i).getDvcId());
			map.put("name", dvcIdList.get(i).getName());
			map.put("chartStatus", dvcIdList.get(i).getChartStatus());
			map.put("operationTime", dvcIdList.get(i).getOperationTime());

			list.add(map);
		}
		;

		Map resultMap = new HashMap();
		resultMap.put("dvcId", list);
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);

		return str;
	}

	
	@Override
	public String getStartTime(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = (String) sql.selectOne(namespace + "getStartTime", chartVo);
		return rtn;
	}
	
	@Override
	public String getComName(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = URLEncoder.encode((String) sql.selectOne(namespace + "getComName", chartVo),"utf-8");
		return str;
	}
	
	
	@Override
	public ChartVo getBanner(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getBanner", chartVo);
		return chartVo;
	}

	@Override
	public String login(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		
		int exist = (int) sql.selectOne(namespace + "login", chartVo);
		
		if(exist==0){
			str = "fail";
		}else{
			str = "success";
		};
		
		return str;
	};
	
	@Override
	public String getAppList(ChartVo chartVo) throws Exception {
		List <ChartVo> dataList  = app_server_sql.selectList(namespace + "getAppList", chartVo);
	    
		List list = new ArrayList<ChartVo>();
		for(int i = 0; i < dataList.size(); i++){
			Map map = new HashMap();
			
			map.put("id", dataList.get(i).getId());
			map.put("appId", dataList.get(i).getAppId());
			map.put("name", dataList.get(i).getAppName());
			map.put("url", dataList.get(i).getUrl());
			
			list.add(map);
		}; 
  
		Map dataMap = new HashMap(); 
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String removeApp(ChartVo chartVo) throws Exception {
		String str = "";
		try{
			app_server_sql.delete(namespace + "removeApp", chartVo);
			str = "success";
		}catch(Exception e){
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}

	@Override
	public String addNewApp(ChartVo chartVo) throws Exception {
		String str = "";
		try{
			app_server_sql.delete(namespace + "addNewApp", chartVo);
			str = "success";
		}catch(Exception e){
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}
	
	//common func
	@Override
	public String getDvcNameList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDvcNameList", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < dataList.size(); i++ ){
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("status", dataList.get(i).getStatus());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(),"utf-8"));
			map.put("lastAlarmCode", dataList.get(i).getLastAlarmCode());
			map.put("lastAlarmMsg", dataList.get(i).getLastAlarmMsg());
			
			list.add(map);
		};
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list); 
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}
	
	@Override
	public ChartVo getCurrentDvcData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getCurrentDvcData", chartVo);
		//chartVo.setEndDateTime((String)sql.selectOne(namespace + "getLastUpdateTime", chartVo));
		 
		if(chartVo==null){
			chartVo = new ChartVo();
			chartVo.setChartStatus("null");
		};

		return chartVo;
	};
	
	@Override
	public String getTimeData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> statusList = sql.selectList(namespace +"getTimeData", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < statusList.size(); i++){
			Map map = new HashMap();
			map.put("status", statusList.get(i).getStatus());
			map.put("startDateTime", statusList.get(i).getStartDateTime());
			map.put("spdLoad", statusList.get(i).getSpdLoad());
			map.put("spdOverride", statusList.get(i).getFeedOverride());
			
			list.add(map);
		};
		
		Map statusMap = new HashMap();
		statusMap.put("statusList", list);
		
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(statusMap);
		return str;
	}
	
	@Override
	public String getDetailBlockData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDetailBlockData", chartVo);
		
		List list = new ArrayList(); 
		for(int i = 0; i < dataList.size(); i++ ){
			Map map = new HashMap();
			map.put("lastAlarmCode", dataList.get(i).getLastAlarmCode());
			map.put("lastAlarmMsg", dataList.get(i).getLastAlarmMsg());
			map.put("lastTgPrdctNum", dataList.get(i).getLastTgPrdctNum());
			map.put("lastFnPrdctNum", dataList.get(i).getLastFnPrdctNum());
			map.put("LastAvrCycleTime", dataList.get(i).getLastAvrCycleTime());
			map.put("tgCnt", dataList.get(i).getTgCnt());
			map.put("status", dataList.get(i).getStatus());
			map.put("prdctPerHour", dataList.get(i).getPrdctPerHour());
			
			map.put("prdctPerCyl", dataList.get(i).getPrdctPerCyl());
			map.put("remainCnt", dataList.get(i).getRemainCnt());
			map.put("feedOverride", dataList.get(i).getFeedOverride());
			map.put("spdLoad", dataList.get(i).getSpdLoad());
			map.put("ncAlarmNum1", dataList.get(i).getNcAlarmNum1());
			map.put("ncAlarmMsg1", URLEncoder.encode(dataList.get(i).getNcAlarmMsg1(),"utf-8"));
			map.put("ncAlarmNum2", dataList.get(i).getNcAlarmNum2());
			map.put("ncAlarmMsg2", URLEncoder.encode(dataList.get(i).getNcAlarmMsg2(),"utf-8"));
			map.put("ncAlarmNum3", dataList.get(i).getNcAlarmNum3());
			map.put("ncAlarmMsg3", URLEncoder.encode(dataList.get(i).getNcAlarmMsg3(),"utf-8"));
		
			 
			map.put("programHeader", dataList.get(i).getProgramHeader());
			map.put("programName", dataList.get(i).getProgramName());
			map.put("type", dataList.get(i).getType());
			list.add(map);
		};
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getAlarmList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getAlarmList", chartVo);
		
		List list = new ArrayList(); 
		for(int i = 0; i < dataList.size(); i++ ){
			Map map = new HashMap();

			map.put("startDateTime", dataList.get(i).getStartDateTime());
			map.put("endDateTime", dataList.get(i).getEndDateTime());
			map.put("alarmMsg", URLEncoder.encode(dataList.get(i).getAlarmMsg(),"UTF-8"));
			map.put("alarmCode", dataList.get(i).getAlarmCode());
			map.put("dvcId", dataList.get(i).getDvcId());
			
			list.add(map);
		};
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public ChartVo getSummaryData(ChartVo chartVo) throws Exception {
		
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getSummaryData", chartVo);
		
		if(chartVo==null){
			chartVo = new ChartVo();
			chartVo.setChartStatus("null");
		};

		return chartVo;
	}
	
	@Override
	public String getBarChartData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> statusList = sql.selectList(namespace +"getBarChartData", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < statusList.size(); i++){
			Map map = new HashMap();
			map.put("status", statusList.get(i).getStatus());
			map.put("startDateTime", statusList.get(i).getStartDateTime());
			map.put("dateDiff", statusList.get(i).getTimeDiff());
			
			list.add(map);
		};
		
		Map statusMap = new HashMap();
		statusMap.put("statusList", list);
		
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(statusMap);
		return str;
	}

	@Override
	public String getSpindleData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> statusList = sql.selectList(namespace +"getSpindleData", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < statusList.size(); i++){
			Map map = new HashMap();
			map.put("startDateTime", statusList.get(i).getStartDateTime());
			map.put("spdLoad", statusList.get(i).getSpdLoad());
			map.put("spdOverride", statusList.get(i).getSpdOverride());
			
			list.add(map);
		};
		
		Map statusMap = new HashMap();
		statusMap.put("statusList", list);
		
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(statusMap);
		return str;
	}

	@Override
	public String getLampData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> statusList = sql.selectList(namespace +"getLampData", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < statusList.size(); i++){
			Map map = new HashMap();
			map.put("status", statusList.get(i).getStatus());
			map.put("startDateTime", statusList.get(i).getStartDateTime());
			map.put("spdLoad", statusList.get(i).getSpdLoad());
			map.put("spdOverride", statusList.get(i).getFeedOverride());
			
			list.add(map);
		};
		
		Map statusMap = new HashMap();
		statusMap.put("statusList", list);
		
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(statusMap);
		return str;
	}

	@Override
	public String getChartStatus(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> statusList = sql.selectList(namespace +"getChartStatus", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < statusList.size(); i++){
			Map map = new HashMap();
			map.put("chartStatus", statusList.get(i).getChartStatus());
			list.add(map);
		};
		
		Map statusMap = new HashMap();
		statusMap.put("statusList", list);
		
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(statusMap);
		return str;
	}

	@Override
	public String getStatus(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> statusList = sql.selectList(namespace +"getStatus", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < statusList.size(); i++){
			Map map = new HashMap();
			map.put("dvcId", statusList.get(i).getDvcId());
			map.put("startDateTime", statusList.get(i).getStartDateTime());
			map.put("endDateTime", statusList.get(i).getEndDateTime());
			map.put("chart_status", statusList.get(i).getChartStatus());
			map.put("spdOverride", statusList.get(i).getSpdOverride());
			map.put("diff", statusList.get(i).getTimeDiff());
			
			list.add(map);
		};
		
		Map statusMap = new HashMap();
		statusMap.put("statusList", list);
		
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(statusMap);
		return str;
	}

	@Override
	public String getSpindleData2(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> statusList = sql.selectList(namespace +"getSpindleData2", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < statusList.size(); i++){
			Map map = new HashMap();
			map.put("startDateTime", statusList.get(i).getStartDateTime());
			map.put("timeDiff", statusList.get(i).getTimeDiff());
			map.put("spdLoad", statusList.get(i).getSpdLoad());
			
			list.add(map);
		};
		
		Map statusMap = new HashMap();
		statusMap.put("statusList", list);
		
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(statusMap);
		return str;
	}

	@Override
	public String getOldSpindleData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> statusList = sql.selectList(namespace +"getOldSpindleData", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < statusList.size(); i++){
			Map map = new HashMap();
			map.put("startDateTime", statusList.get(i).getStartDateTime());
			map.put("timeDiff", statusList.get(i).getTimeDiff());
			map.put("spdLoad", statusList.get(i).getSpdLoad());
			
			list.add(map);
		};
		
		Map statusMap = new HashMap();
		statusMap.put("statusList", list);
		
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(statusMap);
		return str;
	}
};