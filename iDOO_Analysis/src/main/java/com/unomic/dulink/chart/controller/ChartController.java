package com.unomic.dulink.chart.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.service.ChartService;
/**
 * Handles requests for the application home page.
 */
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/")
@Controller
public class ChartController {

	private static final Logger logger = LoggerFactory.getLogger(ChartController.class);
	final int shopId = 1;
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@Autowired
	private ChartService chartService; 
	
	
	@RequestMapping(value="getProductionDaily")
	@ResponseBody
	public String getProductionDaily(ChartVo chartVo) throws Exception {
		String str="";
		try {
			str = chartService.getProductionDaily(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="production_Daily")
	public String production_Daily(){
		return "chart/production_Daily";
	};
	
	@RequestMapping(value="getProductionCnt")
	@ResponseBody
	public String getProductionCnt(ChartVo chartVo){
		String result = "";
		try {
			result = chartService.getProductionCnt(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value = "getTableData")
	@ResponseBody
	public String getTableData(ChartVo chartVo) {
		String result = "";
		try {
			result = chartService.getTableData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="production_Device")
	public String production_Device(){
		return "chart/production_Device";
	};
	
	@RequestMapping(value = "getWcDataByDvc")
	@ResponseBody
	public String getWcDataByDvc(ChartVo chartVo) {
		String result = "";
		try {
			result = chartService.getWcDataByDvc(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};

	@RequestMapping(value = "getMatInfo")
	@ResponseBody
	public String getMatInfo(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getMatInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "getJigList4Report")
	@ResponseBody
	public String getJigList4Report(ChartVo chartVo) {
		String result = "";
		try {
			result = chartService.getJigList4Report(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="Performance_Report_Daily")
	public String Performance_Report_Daily(){
		return "chart/Performance_Report_Daily";
	};
	
	@RequestMapping(value="Performance_Report_Daily_table")
	public String Performance_Report_Daily_table(){
		return "chart/Performance_Report_Daily";
	};
	
	
	@RequestMapping(value="Performance_Report_Chart_table")
	public String Performance_Report_Chart_table(){
		return "chart/Performance_Report_Chart_table";
	};
	
	@RequestMapping(value="Performance_Report_Chart")
	public String alarmReport(){
		return "chart/Performance_Report_Chart";
	};
	
	@RequestMapping(value="getAlarmList")
	@ResponseBody
	public String getAlarmList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getAlarmList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return str;
	}
	
	@RequestMapping(value = "getBarChartDvcId")
	@ResponseBody
	public String getBarChartDvcId2(ChartVo chartVo) {
		String result = null;
		try {
			result = chartService.getBarChartDvcId(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};

	
	@RequestMapping(value="getDvcNameList")
	@ResponseBody
	public String getDvcNameList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getDvcNameList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="	getDetailBlockData")
	@ResponseBody
	public String 	getDetailBlockData(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.	getDetailBlockData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	
	@RequestMapping(value="getTimeData")
	@ResponseBody
	public String getTimeData(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getTimeData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getLampData")
	@ResponseBody
	public String getLampData(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getLampData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getBarChartData")
	@ResponseBody
	public String getBarChartData(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getBarChartData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	
	@RequestMapping(value="getCurrentDvcData")
	@ResponseBody
	public ChartVo getCurrentDvcData(ChartVo chartVo){
		try {
			Calendar cal = Calendar.getInstance ( );//�삤�뒛 �궇吏쒕�� 湲곗��쑝猷�..
			//cal.add ( cal.DATE, 1); //2媛쒖썡 �쟾....
			
			String year = String.valueOf(cal.get ( cal.YEAR ));
			String month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
			String date = addZero(String.valueOf(cal.get ( cal.DATE )));
			
			String hour = String.valueOf(cal.get(cal.HOUR_OF_DAY));
			String minute = String.valueOf(cal.get(cal.MINUTE));
			String second = String.valueOf(cal.get(cal.SECOND));
			chartVo.setDate(year + "-" + month + "-" + date);
			
			if(Integer.parseInt(hour)>=20){
				cal.add ( cal.DATE, 1);
				year = String.valueOf(cal.get ( cal.YEAR ));
				month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
				date = addZero(String.valueOf(cal.get ( cal.DATE )));
				
				chartVo.setDate(year + "-" + month + "-" + date);
			};
			
			chartVo = chartService.getCurrentDvcData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		return chartVo;
	};
	
	public String addZero(String str){
		String rtn = str;
		if(rtn.length()==1){
			rtn = "0" + str;
		};
		return rtn;
	};
	
	//common func
	
	@RequestMapping(value="getAppList")
	@ResponseBody
	public String getAppList(ChartVo chartVo){
		String str = ""; 
		try {
			str = chartService.getAppList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return str;
	}
	
	
	@RequestMapping(value="removeApp")
	@ResponseBody
	public String removeApp(ChartVo chartVo){
		String str = ""; 
		try {
			str = chartService.removeApp(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return str;
	}
	
	float progress = 0;
	
	@RequestMapping(value = "resetProgress")
	@ResponseBody
	public void resetProgress(){
		progress = 0;
	};
	
	@RequestMapping(value = "getProgress")
	@ResponseBody
	public float getProgress(){
		//System.out.println(progress);
		return progress;
	};
	
	@RequestMapping(value = "fileDown")
	@ResponseBody
	public String sample(ChartVo chartVo) throws Exception{
		String fileLocation = chartVo.getFileLocation();
        String fileName = fileLocation.substring(fileLocation.lastIndexOf("/") + 1);
        
        File file = new File(System.getProperty( "catalina.base" ) + "/webapps/" + fileName);
        //File file = new File("/Users/jeongwan/Desktop/" + fileName);
        URL url = new URL(fileLocation);
        
        progress = 0;
        System.out.println(file);
        String str = "";
      //  try (FileOutputStream fos = new FileOutputStream(file)){
        try{
            HttpURLConnection httpConnection = (HttpURLConnection) (url.openConnection());
            float completeFileSize = httpConnection.getContentLength();
            
        	//FileUtils.copyURLToFile(url, file);

        	BufferedInputStream in = new BufferedInputStream(httpConnection.getInputStream());
        	byte[] data = new byte[1024];
            float downloadedFileSize = 0;
            float x = 0;
            while ((x = in.read(data, 0, 1024)) >= 0) {
                downloadedFileSize += x;
                progress = (downloadedFileSize/completeFileSize)*100;
            }
            
            chartService.addNewApp(chartVo);
            in.close();
            
            str = "success";
        } catch (Exception e){
        	e.printStackTrace();
        	str = "fail";
        }
        
        return str;
	};
	
	@RequestMapping(value="login")
	@ResponseBody 
	public String loginCnt(ChartVo chartVo){
		String str = ""; 
		try {
			str = chartService.login(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return str;
	}
	
	
	@RequestMapping(value="getStartTime")
	@ResponseBody
	public String getStartTime(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getStartTime(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	
	@RequestMapping(value="getComName")
	@ResponseBody 
	public String getComName(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getComName(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getBanner")
	@ResponseBody
	public ChartVo getBanner(ChartVo chartVo){
		try {
			chartVo = chartService.getBanner(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return chartVo;
	}
	
	@RequestMapping(value="getSummaryData")
	@ResponseBody
	public ChartVo getSummaryData(ChartVo chartVo){
		try {
			chartVo = chartService.getSummaryData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		return chartVo;
	};
	
	@RequestMapping(value="getSpindleData")
	@ResponseBody
	public String getSpindleData(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getSpindleData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getChartStatus")
	@ResponseBody
	public String getChartStatus(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getChartStatus(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getStatus")
	@ResponseBody
	public String getStatus(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getStatus(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getSpindleData2")
	@ResponseBody
	public String getSpindleData2(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getSpindleData2(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getOldSpindleData")
	@ResponseBody
	public String getOldSpindleData(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getOldSpindleData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value = "csv")
	public String csv(String csv) {
		try{
		}catch(Exception e){
			e.printStackTrace();
		}
		return "csv/csv1"; 
	}
	
};

